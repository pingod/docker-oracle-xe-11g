create table TEST
(
  A VARCHAR2(20),
  B VARCHAR2(50)
)
;

insert into TEST (A, B) values ('111', '111');
insert into TEST (A, B) values ('222', '222');
commit;